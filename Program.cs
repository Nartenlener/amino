﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amino
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Int32> kupony;
            Dictionary<int, int> wykorzystane = new Dictionary<int, int>(); 

            int skladnik1 = 0;
            int skladnik2 = 0;
            int skladnik3 = 0;
            int skladnik4 = 0;
            int skladnik5 = 0;

            int suma = 0;
            

            string kuponyTxt = System.IO.File.ReadAllText(@"..\..\..\Amino\Kupony.txt");

            kupony = kuponyTxt.Split(new String[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList<int>();

            Console.WriteLine("START!");

            for (int i = 0; i < kupony.Count(); i++)
            {
                skladnik1 = kupony[i];

                // Dla dwóch skladników
                for (int j = 0; j < kupony.Count(); j++) 
                {
                    // nie mozna brac dwa razy tego samego kuponu
                    if (i != j)
                    {
                        skladnik2 = kupony[j];
                        suma = skladnik1 + skladnik2;

                        if (SprawdzWarunekPromocji(suma))
                        {
                            Console.WriteLine("(Pozycja na liście: " + i + ") " + skladnik1 + " + " + " (" + j + ") " + skladnik2 + " = " + suma);
                        }
                    }

                    // Dla 3 skladnikow
                    for (int k = 0; k < kupony.Count(); k++)
                    {
                        // nie mozna brac dwa razy tego samego kuponu
                        if (i != j && i != k && j != k)
                        {
                            skladnik3 = kupony[k];
                            suma = skladnik1 + skladnik2 + skladnik3;

                            if (SprawdzWarunekPromocji(suma))
                            {
                                Console.WriteLine("(Pozycja na liście: " + i + ") " + skladnik1 + " + " 
                                    + " (" + j + ") " + skladnik2 
                                    + " (" + k + ") " + skladnik3 
                                    + " = " + suma);
                            }
                        }

                        // Dla 4 skladnikow
                        for (int l = 0; l < kupony.Count(); l++)
                        {
                            // nie mozna brac dwa razy tego samego kuponu
                            if (i != j && i != k && j != k && j != l && i != l && k != l)
                            {
                                skladnik4 = kupony[l];
                                suma = skladnik1 + skladnik2 + skladnik3 + skladnik4;

                                if (SprawdzWarunekPromocji(suma))
                                {
                                    Console.WriteLine("(Pozycja na liście: " + i + ") " + skladnik1 + " + "
                                        + " (" + j + ") " + skladnik2
                                        + " (" + k + ") " + skladnik3
                                        + " (" + l + ") " + skladnik4
                                        + " = " + suma);
                                }
                            }

                            // Dla 5 skladnikow
                            for (int m = 0; m < kupony.Count(); m++)
                            {
                                // nie mozna brac dwa razy tego samego kuponu
                                if (i != j && i != k && j != k && j != l && i != l && k != l && i != m && j != m && k != m && l != m)
                                {
                                    skladnik5 = kupony[m];
                                    suma = skladnik1 + skladnik2 + skladnik3 + skladnik4 + skladnik5;

                                    if (SprawdzWarunekPromocji(suma))
                                    {
                                        Console.WriteLine("(Pozycja na liście: " + i + ") " + skladnik1 + " + "
                                            + " (" + j + ") " + skladnik2
                                            + " (" + k + ") " + skladnik3
                                            + " (" + l + ") " + skladnik4
                                            + " (" + m + ") " + skladnik5
                                            + " = " + suma);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine("KONIEC!");
            Console.ReadLine();
        }

        public static bool SprawdzWarunekPromocji(int suma)
        {
            if ((suma % 100) == 0 && suma < 1000)
            {
                return true;
            }

            return false;
        }
    }
}
